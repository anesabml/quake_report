package com.example.android.quakereport;

/**
 * An {@link Earthquake} object contains information related to a single earthquake.
 */

public class Earthquake {

    /** Magnitude of the earthquake */
    private double  mMagnitude;

    /** Location of the earthquake */
    private String mLocation;

    /** Time of the earthquake */
    private long mTimeInMilliseconds;

    /** Url of the earthquake */
    private String mUrl;

    /**
     * Create a new {@link Earthquake} object.
     *
     * @param magnitude is the magnitude of the earthquake.
     * @param location is the location of the earthquake.
     * @param timeInMilliseconds is the time in milliseconds (from the Epoch) when
     *                           the earthquake happened
     */
    public Earthquake(double magnitude, String location, long timeInMilliseconds, String url){
        mMagnitude = magnitude;
        mLocation = location;
        mTimeInMilliseconds = timeInMilliseconds;
        mUrl = url;
    }

    /**
     * Return the magnitude of the earthquake.
     */
    public double getMagnitude() {
        return mMagnitude;
    }

    /**
     * Return the location of the earthquake.
     */
    public String getLocation() {
        return mLocation;
    }

    /**
     * Return the time of the earthquake.
     */
    public long getTimeInMilliseconds() {
        return mTimeInMilliseconds;
    }

    /**
     * Return the URL of the earthquake.
     */
    public String getUrl() {
        return mUrl;
    }
}
